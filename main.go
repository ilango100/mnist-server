package main

import (
	"fmt"
	"log"
	"sync"

	tf "github.com/tensorflow/tensorflow/tensorflow/go"
)

var currentModel *tf.SavedModel
var currentModelDir string
var lock sync.RWMutex
var exit chan bool
var input tf.Output
var output tf.Output

func main() {
	fmt.Println("MNIST Server...")

	fmt.Print("Loading Model...")
	if err := loadModel(); err != nil {
		log.Fatalln("Error loading model:", err)
	}
	fmt.Println("OK")

	fmt.Println("Starting HTTP server at port 9000...")
	go startHTTPServer()

	<-exit
}
