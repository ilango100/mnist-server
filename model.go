package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"reflect"

	tf "github.com/tensorflow/tensorflow/tensorflow/go"
)

func loadModel() error {

	if currentModelDir != "" {
		log.Println("Current loaded model:", currentModelDir)
		log.Println("Searching for updates...")
	}

	models, err := ioutil.ReadDir("models")
	if err != nil {
		return err
	}
	latest := models[0]
	for _, v := range models {
		if v.ModTime().After(latest.ModTime()) {
			latest = v
		}
	}
	if latest.Name() != currentModelDir {
		currentModelDir = latest.Name()
		log.Println("Update found:", currentModelDir)
	} else {
		log.Println("No updates found....")
		return nil
	}

	log.Println("Loading Updated Model...")
	lock.Lock()
	currentModel, err = tf.LoadSavedModel("models/"+currentModelDir, []string{"serve"}, nil)
	if err == nil {
		lock.Unlock()
	}

	input = currentModel.Graph.Operation("image").Output(0)
	output = currentModel.Graph.Operation("digit").Output(0)

	return err
}

//Classify classifies the image into digit
func Classify(image [][][]float32) string {
	log.Println(len(image), len(image[0]), len(image[0][0]))
	inp, err := tf.NewTensor([][][][]float32{image})
	if err != nil {
		log.Println("Tensor creation error:", err)
		return fmt.Sprint("{digit:-1,error:", err, "}")
	}

	outs, err := currentModel.Session.Run(map[tf.Output]*tf.Tensor{input: inp}, []tf.Output{output}, nil)
	if err != nil {
		log.Println("Error while running model:", err)
		return fmt.Sprint("{digit:-1,error:", err, "}")
	}

	val := outs[0].Value()
	var ival []int64
	if reflect.SliceOf(reflect.TypeOf(int64(2))) == reflect.TypeOf(val) {
		ival = val.([]int64)
		return fmt.Sprint("{digit:", ival[0], "}")
	}

	return fmt.Sprint("{digits:", val, "}")
}
