package main

import (
	"image"
	_ "image/jpeg"
	_ "image/png"
	"log"
	"net/http"

	"github.com/nfnt/resize"
)

func handleRoot(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Mnist Server\nPost request in png format to /digit api endpoint."))
}

func handleUpdate(w http.ResponseWriter, r *http.Request) {
	err := loadModel()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Error loading model: " + err.Error()))
	}
}

func handleImg(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Please POST the image"))
		return
	}

	if r.Header.Get("Content-Type") != "image/png" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Image MIME type must be of png" + r.Header.Get("Content-Type")))
		return
	}

	img, format, err := image.Decode(r.Body)
	if err != nil {
		log.Println("Error parsing image:", format, err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Error while reading image: " + err.Error()))
		return
	}

	log.Println("Image received and parsed:", format)
	bounds := img.Bounds()
	if bounds.Max.X != 28 || bounds.Max.Y != 28 {
		img = resize.Resize(28, 28, img, resize.NearestNeighbor)
		bounds = img.Bounds()
		if bounds.Max.X != 28 || bounds.Max.Y != 28 {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("Error while resizing the image"))
			return
		}
	}

	gimg, ok := img.(*image.Gray)
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Image must be in grayscale"))
		return
	}

	imgArr := make([][][]float32, bounds.Max.Y)
	for i := 0; i < bounds.Max.Y; i++ {
		imgArr[i] = make([][]float32, bounds.Max.X)
		for j := 0; j < bounds.Max.X; j++ {
			imgArr[i][j] = []float32{float32(gimg.GrayAt(j, i).Y)}
		}
	}

	w.Write([]byte(Classify(imgArr)))
}

func startHTTPServer() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", handleRoot)
	mux.HandleFunc("/digit", handleImg)
	mux.HandleFunc("/update", handleUpdate)
	http.ListenAndServe(":9000", mux)
}
